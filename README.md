# Data 
John Menerick's private MISP instance with corresponding COINTEL datasets

# Indicators 
## Attribution
![alt text][attribution]

[attribution]: https://github.com/cloudsriseup/IntelMetrics/raw/master/attributeschart.PNG "Attribution Chart"

## Tags
![alt text][tags]

[tags]: https://github.com/cloudsriseup/IntelMetrics/raw/master/tagtreechart.PNG "tags tree chart"



## Usage and Behaviors
![alt text][usage]

[usage]: https://github.com/cloudsriseup/IntelMetrics/raw/master/usageandbehaviors.PNG "Usage and behaviors chart"
